from concurrent.futures import ThreadPoolExecutor

from config import config


class ThreadPool:
    executor = ThreadPoolExecutor(max_workers=config.MAX_SERVER_WORKER)

    def __init__(self):
        self.executor = ThreadPool.executor

    def execute(self, func, *args, **kwargs):
        self.executor.submit(func, *args, **kwargs)
