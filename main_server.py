from tcp import TCPServer, TCPHandler
from thread_pool import ThreadPool
from config import config


def main():
    thread_pool = ThreadPool()
    tcp_handler = TCPHandler()
    server = TCPServer((config.SERVER_HOST, config.TCP_SERVER_PORT), tcp_handler, thread_pool)
    run(server)


def run(server):
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()


if __name__ == '__main__':
    main()
