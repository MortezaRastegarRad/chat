from abc import ABC


class BaseServer(ABC):
    def serve_forever(self):
        raise NotImplemented

    def server_close(self):
        raise NotImplemented
