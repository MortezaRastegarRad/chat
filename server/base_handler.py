from abc import ABC


class BaseHandler(ABC):
    def handel(self, connection, data):
        raise NotImplemented
