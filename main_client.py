import logging
from threading import Thread
from time import perf_counter
import socket
from config.config import SERVER_HOST, TCP_SERVER_PORT

logging.basicConfig(level=logging.DEBUG)


class Client:
    @classmethod
    def run(cls):
        start = perf_counter()
        thread_list = list()
        client_number = 20
        for i in range(client_number):
            thread_list.append(Thread(target=cls.call_server, args=(i,)))
        for i in range(client_number):
            thread_list[i].start()
        for i in range(client_number):
            thread_list[i].join()
        end = perf_counter()
        logging.debug(f"It took {end - start} second(s) to finish.")

    @classmethod
    def call_server(cls, i):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((SERVER_HOST, TCP_SERVER_PORT))
            s.sendall(f"{i}".encode())
            data = s.recv(1024)
            logging.debug(f"{data.decode()}")


if __name__ == '__main__':
    Client().run()
