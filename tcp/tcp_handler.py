import logging
import time

from server import BaseHandler

logging.basicConfig(level=logging.DEBUG)


class TCPHandler(BaseHandler):

    def handel(self, connection, client_id: str):
        time.sleep(5)
        connection.send(f"hi number {client_id}".encode())
        logging.debug(f"Send to {client_id}")
        connection.close()
