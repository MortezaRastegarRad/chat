import logging
import socket
from typing import Tuple

from .tcp_handler import TCPHandler
from thread_pool import ThreadPool
from server import BaseServer

logging.basicConfig(level=logging.DEBUG)


class TCPServer(BaseServer):
    def __init__(self, socket_address: Tuple[str, str], handler: TCPHandler, thread_pool: ThreadPool):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host, self.port = socket_address
        self.thread_pool = thread_pool
        self.handler = handler

    def serve_forever(self):
        self.socket.bind((self.host, self.port))
        self.socket.listen()
        while True:
            conn, addr = self.socket.accept()
            data = conn.recv(1024)
            logging.debug(f"Received from {data.decode()}")
            self.thread_pool.execute(self.handler.handel, conn, data.decode())

    def server_close(self):
        self.socket.close()
        logging.debug("Server stopped.")
