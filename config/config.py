SERVER_HOST = "127.0.0.1"  # Server listen host
TCP_SERVER_PORT = 65432  # Server listen port
MAX_SERVER_WORKER = 20  # maximum number of thread that run in server for handling tcp connection
